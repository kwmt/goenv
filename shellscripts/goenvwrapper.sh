#!/usr/bin/env bash
# Copyright 2012 Yoshifumi Yamaguchi, All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.
#
#
# goenvwrapper.sh -- goenv utility wrapper functions
#
# This script provides "goof" command, which is army knife command set for goenv.
#
# To use "goof" command, add following line in your .bashrc:
#
#   source /path/to/goenvwrapper.sh
#
# install.sh installs this script into the same install target as 'goenv'.
# Default is '/usr/local/bin'.

declare -x GOENVHOME

# main command
typeset -f goof

# private functions
typeset -f _goof_show
typeset -f _goof_make
typeset -f _goof_remove
typeset -f _goof_workon
typeset -f _goof_showgo
typeset -f _goof_goinstall
typeset -f _goof_go
typeset -f _goof_usage
typeset -f _goof_validator

# alias to rm
_RM=/bin/rm

function goof() {
  case "$1" in
    "show")
      shift
      _goof_show
      ;;
    "workon")
      shift
      _goof_workon $*
      ;;
    "make")
      shift
      _goof_make $*
      ;;
    "remove")
      shift
      _goof_remove $*
      ;;
    "goinstall")
      shift
      _goof_goinstall $*
      ;;
    "go")
      shift
      _goof_go $*
      ;;
    *)
      _goof_usage $*
      ;;
  esac
}

function _goof_usage() {
  cat <<"EOF"
goof -- goenv wrapper for goofy gophers

usage: goof make [goenv options] <envname>
       goof workon <envname>
       goof remove <envname>
       goof show
       goof goinstall [-u] <version>
       goof go [version]

a list of goof commands:
 make		Create a new Go workspace
 workon		Switch the environment to specified workspace
 remove		Delete a specified workspace
 show		Show a list of existing workspaces
 goinstall	Build and install Go binary of given version
 go			Replace GOROOT with specified Go version binaries
EOF
  return 0
}


# goof_validator checks all requred enviroment variables
function _goof_validator() {
  if [ -z "$GOENVHOME" ]; then
    echo "please set environment variable 'GOENVHOME' to use goenvwrapper!"
    return 1
  fi
  if [ ! -x "$(which goenv)" ]; then
    echo "please put binary 'goenv' under PATH to use goenvwrapper!"
    return 1
  fi
  return 0
}


# goof_show returns all environment under $GOENVHOME
# TODO(ymotongpoo): check existance of `activate` script.
function _goof_show() {
  if [ `_goof_validator = 0` ] ; then
    return 1
  fi
  for dir in "$GOENVHOME"/*; do
    if [ -d "$dir" ]; then
      local envname=${dir##*/}
      local prefix="  "
      if [ "$envname" = "$GOENVNAME" ]; then
        prefix="* "
      fi
      echo "$prefix$envname"
      unset envname
      unset prefix
    fi
  done
  return 0
}

# goof_make is short of normal 'goenv' command.
# all options are passed to goenv, but a target directory is
# going to be under $GOENVHOME.
function _goof_make() {
  if [ -z "$1" ]; then
    _goof_usage
    return 1
  fi

  if [ `_goof_validator = 0` ] ; then
    return 1
  fi

  local envhome
  local args
  until [ -z "$1" ]; do
    case "$1" in
      "-go" | "--go")
        if [ -n "$GOENVGOROOT" ]; then
          if [ -d "$GOENVGOROOT/$2" ]; then
            args="$args $1 $GOENVGOROOT/$2"
            shift 2
          else
            echo "[goof] $GOENVGOROOT/$2 does not exist."
            return 1
          fi
        else
          args="$args $1 $2"
          shift 2
        fi
        ;;
      "-deps" | "--deps")
        args="$args $1 $2"
        shift 2
        ;;
      "-gae")
        args="$args -gae"
        shift
        ;;
      *)
        envhome="$GOENVHOME/$1"
        shift
        ;;
    esac
  done
  eval "goenv $args $envhome"
  if [ -f "$envhome/activate" ]; then
    source "$envhome/activate"
  fi
  cd "$envhome"
  return 0
}

# goof_remove delets given workspace.
function _goof_remove() {
  if [ -z "$1" ]; then
    _goof_usage
    return 0
  fi

  if [ `_goof_validator = 0` ] ; then
    return 1
  fi

  local envhome="$GOENVHOME/$1"
  if [ -d "$envhome" ]; then
    if [ "$GOENVNAME" = "$1" ]; then
      deactivate
    fi

    if [ "$PWD" = "$envhome" ]; then
      cd "$GOENVHOME"
    fi
    eval `"$_RM" -rf "$envhome"`
  else
    echo "confirm 'GOENVHOME' or envname"
    echo "GOENVHOME=$GOENVHOME"
    goof_show
    return 1
  fi
  return 0
}

# goof_workon is short cut for activating workspace and 
# changing directory to the workspace.
function _goof_workon() {
  if [ -z "$1" ]; then
    _goof_show
    return 0
  fi

  if [ `_goof_validator = 0` ] ; then
    return 1
  fi

  if [ -n "$1" ]; then
    if [ -f "$GOENVHOME/$1/activate" ]; then
      source "$GOENVHOME/$1/activate"
      cd "$GOENVHOME/$1"
    fi
  fi
  return 0
}


function _goof_showgo() {
  # if no argument given, it shows available Go versions
  for dir in "$GOENVGOROOT"/*; do
    if [ -d "$dir" ]; then
      local prefix="  "
      if [ "$dir" = "$GOROOT" ]; then
        prefix="* "
      fi
      if [ "$dir" = "$GOENVGOROOT/go" ]; then
        dir="$dir (repository)"
      fi
      echo "$prefix$dir"
      unset prefix
    fi
  done
  return 0
}


# goof_goinstall installs Go with specific version
function _goof_goinstall() {
  # check required environment variables are set
  if [ -z "$GOENVGOROOT" ]; then
    echo "[goof] set enviroment variable GOENVGOROOT to use 'goof goinstall' command"
    return 1
  fi

  if [ ! -d "$GOENVGOROOT" ]; then
    echo "[goof] no such directory: $GOENVGOROOT"
    return 1
  fi

  if [ -z "$1" ]; then
    _goof_showgo
    return 0
  fi
  # check uninstall command
  if [ "$1" = "-u" ]; then
    if [ -d "$GOENVGOROOT/$2" ]; then
      eval `$_RM -rf "$GOENVGOROOT/$2"`
      echo "[goof] uninstalled version '$2'"
      return 0
    else
      echo "[goof] cannot unintall version '$2'. not installed."
      return 1
    fi
  fi

  # check mercurial
  if ! type hg > /dev/null; then
    echo "[goof] 'goof goinstall' requires mercurial."
    echo "[goof] Install it from http://mercurial.selenic.com/"
    return 1
  fi

  # build and copy directory
  if [ -d "$GOENVGOROOT/$1" ]; then
    echo "[goof] Go version '$1' already exists."
  else
    local repo=$GOENVGOROOT/go
    # initial checkout
    echo "[goof] check repository existence."
    if [ ! -d "$repo" ]; then
      echo "[goof] Go source code initial checkout."
      eval "hg clone -q https://code.google.com/p/go/ --cwd $GOENVGOROOT"
    fi
    cd $repo
    if [ ! -d "$repo/logs" ]; then
      mkdir "$repo/logs"
    fi

    # check tags
    if [ "$1" = "tags" ]; then
      hg tags
      return 0
    fi

    # build and copy
    local datetime=`date +"%Y%m%d%H%M%S"`
    local logfile="$repo/logs/$1-$datetime.log"
    if eval "hg checkout $1 &> $logfile"; then
      cd src
      echo "[goof] building '$1'."
      if ./all.bash &>> "$logfile"; then
        cd "$repo"
        echo "[goof] copying '$1'."
        if rsync -av --exclude='.hg*' . "$GOENVGOROOT/$1" &>> "$logfile"; then
          echo "[goof] '$1' is installed in $GOENVGOROOT/$1"
        fi
      else
        echo "see build log file: $logfile"
        return 1
      fi
    else
      echo "check tag name: $1"
    fi
  fi
}

# goof_go changes current GOROOT in specified version
function _goof_go() {
  if [ -z "$GOENVGOROOT" ]; then
    echo "[goof] set enviroment variable GOENVGOROOT to use 'goof go' command"
    return 1
  fi
  if [ -z "$1" -o ! -d "$GOENVGOROOT/$1" ]; then
    _goof_showgo
    return 1
  fi

  export _PREV_GOROOT="$GOROOT"
  export GOROOT="$GOENVGOROOT/$1"
  echo "[goof] changed GOROOT=$GOENVGOROOT/$1"
  return 0
}

